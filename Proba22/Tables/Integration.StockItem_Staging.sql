SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Integration].[StockItem_Staging] (
		[Stock Item Staging Key]       [int] IDENTITY(1, 1) NOT NULL,
		[WWI Stock Item ID]            [int] NOT NULL,
		[Stock Item]                   [nvarchar](100) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[Color]                        [nvarchar](20) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[Selling Package]              [nvarchar](50) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[Buying Package]               [nvarchar](50) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[Brand]                        [nvarchar](50) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[Size]                         [nvarchar](20) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[Lead Time Days]               [int] NOT NULL,
		[Quantity Per Outer]           [int] NOT NULL,
		[Is Chiller Stock]             [bit] NOT NULL,
		[Barcode]                      [nvarchar](50) COLLATE Latin1_General_100_CI_AS NULL,
		[Tax Rate]                     [decimal](18, 3) NOT NULL,
		[Unit Price]                   [decimal](18, 2) NOT NULL,
		[Recommended Retail Price]     [decimal](18, 2) NULL,
		[Typical Weight Per Unit]      [decimal](18, 3) NOT NULL,
		[Photo]                        [varbinary](max) NULL,
		[Valid From]                   [datetime2](7) NOT NULL,
		[Valid To]                     [datetime2](7) NOT NULL,
		CONSTRAINT [PK_Integration_StockItem_Staging]
		PRIMARY KEY
		CLUSTERED
		([Stock Item Staging Key])
	ON [USERDATA]
) ON [USERDATA] TEXTIMAGE_ON [USERDATA]
GO
CREATE NONCLUSTERED INDEX [IX_Integration_StockItem_Staging_WWI_Stock_Item_ID]
	ON [Integration].[StockItem_Staging] ([WWI Stock Item ID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Allows quickly locating by WWI Stock Item ID', 'SCHEMA', N'Integration', 'TABLE', N'StockItem_Staging', 'INDEX', N'IX_Integration_StockItem_Staging_WWI_Stock_Item_ID'
GO
EXEC sp_addextendedproperty N'Description', N'Barcode for this stock item', 'SCHEMA', N'Integration', 'TABLE', N'StockItem_Staging', 'COLUMN', N'Barcode'
GO
EXEC sp_addextendedproperty N'Description', N'Brand for the stock item (if the item is branded)', 'SCHEMA', N'Integration', 'TABLE', N'StockItem_Staging', 'COLUMN', N'Brand'
GO
EXEC sp_addextendedproperty N'Description', N'Usual package for selling outers of this stock item (ie cartons, boxes, etc.)', 'SCHEMA', N'Integration', 'TABLE', N'StockItem_Staging', 'COLUMN', N'Buying Package'
GO
EXEC sp_addextendedproperty N'Description', N'Color (optional) for this stock item', 'SCHEMA', N'Integration', 'TABLE', N'StockItem_Staging', 'COLUMN', N'Color'
GO
EXEC sp_addextendedproperty N'Description', N'Does this stock item need to be in a chiller?', 'SCHEMA', N'Integration', 'TABLE', N'StockItem_Staging', 'COLUMN', N'Is Chiller Stock'
GO
EXEC sp_addextendedproperty N'Description', N'Number of days typically taken from order to receipt of this stock item', 'SCHEMA', N'Integration', 'TABLE', N'StockItem_Staging', 'COLUMN', N'Lead Time Days'
GO
EXEC sp_addextendedproperty N'Description', N'Photo of the product', 'SCHEMA', N'Integration', 'TABLE', N'StockItem_Staging', 'COLUMN', N'Photo'
GO
EXEC sp_addextendedproperty N'Description', N'Quantity of the stock item in an outer package', 'SCHEMA', N'Integration', 'TABLE', N'StockItem_Staging', 'COLUMN', N'Quantity Per Outer'
GO
EXEC sp_addextendedproperty N'Description', N'Recommended retail price for this stock item', 'SCHEMA', N'Integration', 'TABLE', N'StockItem_Staging', 'COLUMN', N'Recommended Retail Price'
GO
EXEC sp_addextendedproperty N'Description', N'Usual package for selling units of this stock item', 'SCHEMA', N'Integration', 'TABLE', N'StockItem_Staging', 'COLUMN', N'Selling Package'
GO
EXEC sp_addextendedproperty N'Description', N'Size of this item (eg: 100mm)', 'SCHEMA', N'Integration', 'TABLE', N'StockItem_Staging', 'COLUMN', N'Size'
GO
EXEC sp_addextendedproperty N'Description', N'Row ID within the staging table', 'SCHEMA', N'Integration', 'TABLE', N'StockItem_Staging', 'COLUMN', N'Stock Item Staging Key'
GO
EXEC sp_addextendedproperty N'Description', N'Full name of a stock item (but not a full description)', 'SCHEMA', N'Integration', 'TABLE', N'StockItem_Staging', 'COLUMN', N'Stock Item'
GO
EXEC sp_addextendedproperty N'Description', N'Tax rate to be applied', 'SCHEMA', N'Integration', 'TABLE', N'StockItem_Staging', 'COLUMN', N'Tax Rate'
GO
EXEC sp_addextendedproperty N'Description', N'Typical weight for one unit of this product (packaged)', 'SCHEMA', N'Integration', 'TABLE', N'StockItem_Staging', 'COLUMN', N'Typical Weight Per Unit'
GO
EXEC sp_addextendedproperty N'Description', N'Selling price (ex-tax) for one unit of this product', 'SCHEMA', N'Integration', 'TABLE', N'StockItem_Staging', 'COLUMN', N'Unit Price'
GO
EXEC sp_addextendedproperty N'Description', N'Valid from this date and time', 'SCHEMA', N'Integration', 'TABLE', N'StockItem_Staging', 'COLUMN', N'Valid From'
GO
EXEC sp_addextendedproperty N'Description', N'Valid until this date and time', 'SCHEMA', N'Integration', 'TABLE', N'StockItem_Staging', 'COLUMN', N'Valid To'
GO
EXEC sp_addextendedproperty N'Description', N'Numeric ID used for reference to a stock item within the WWI database', 'SCHEMA', N'Integration', 'TABLE', N'StockItem_Staging', 'COLUMN', N'WWI Stock Item ID'
GO
EXEC sp_addextendedproperty N'Description', N'Stock item staging table', 'SCHEMA', N'Integration', 'TABLE', N'StockItem_Staging', NULL, NULL
GO
ALTER TABLE [Integration].[StockItem_Staging] SET (LOCK_ESCALATION = TABLE)
GO
