SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [Integration].[Movement_Staging] (
		[Movement Staging Key]              [bigint] IDENTITY(1, 1) NOT NULL,
		[Date Key]                          [date] NULL,
		[Stock Item Key]                    [int] NULL,
		[Customer Key]                      [int] NULL,
		[Supplier Key]                      [int] NULL,
		[Transaction Type Key]              [int] NULL,
		[WWI Stock Item Transaction ID]     [int] NULL,
		[WWI Invoice ID]                    [int] NULL,
		[WWI Purchase Order ID]             [int] NULL,
		[Quantity]                          [int] NULL,
		[WWI Stock Item ID]                 [int] NULL,
		[WWI Customer ID]                   [int] NULL,
		[WWI Supplier ID]                   [int] NULL,
		[WWI Transaction Type ID]           [int] NULL,
		[Last Modifed When]                 [datetime2](7) NULL,
		CONSTRAINT [PK_Integration_Movement_Staging]
		PRIMARY KEY
		CLUSTERED
		([Movement Staging Key])
	ON [USERDATA]
) ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Customer (if applicable)', 'SCHEMA', N'Integration', 'TABLE', N'Movement_Staging', 'COLUMN', N'Customer Key'
GO
EXEC sp_addextendedproperty N'Description', N'Transaction date', 'SCHEMA', N'Integration', 'TABLE', N'Movement_Staging', 'COLUMN', N'Date Key'
GO
EXEC sp_addextendedproperty N'Description', N'When this row was last modified', 'SCHEMA', N'Integration', 'TABLE', N'Movement_Staging', 'COLUMN', N'Last Modifed When'
GO
EXEC sp_addextendedproperty N'Description', N'DW key for a row in the Movement fact', 'SCHEMA', N'Integration', 'TABLE', N'Movement_Staging', 'COLUMN', N'Movement Staging Key'
GO
EXEC sp_addextendedproperty N'Description', N'Quantity of stock movement (positive is incoming stock, negative is outgoing)', 'SCHEMA', N'Integration', 'TABLE', N'Movement_Staging', 'COLUMN', N'Quantity'
GO
EXEC sp_addextendedproperty N'Description', N'Stock item for this purchase order', 'SCHEMA', N'Integration', 'TABLE', N'Movement_Staging', 'COLUMN', N'Stock Item Key'
GO
EXEC sp_addextendedproperty N'Description', N'Supplier (if applicable)', 'SCHEMA', N'Integration', 'TABLE', N'Movement_Staging', 'COLUMN', N'Supplier Key'
GO
EXEC sp_addextendedproperty N'Description', N'Type of transaction', 'SCHEMA', N'Integration', 'TABLE', N'Movement_Staging', 'COLUMN', N'Transaction Type Key'
GO
EXEC sp_addextendedproperty N'Description', N'Customer ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Movement_Staging', 'COLUMN', N'WWI Customer ID'
GO
EXEC sp_addextendedproperty N'Description', N'Invoice ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Movement_Staging', 'COLUMN', N'WWI Invoice ID'
GO
EXEC sp_addextendedproperty N'Description', N'Purchase order ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Movement_Staging', 'COLUMN', N'WWI Purchase Order ID'
GO
EXEC sp_addextendedproperty N'Description', N'Stock Item ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Movement_Staging', 'COLUMN', N'WWI Stock Item ID'
GO
EXEC sp_addextendedproperty N'Description', N'Stock item transaction ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Movement_Staging', 'COLUMN', N'WWI Stock Item Transaction ID'
GO
EXEC sp_addextendedproperty N'Description', N'Supplier ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Movement_Staging', 'COLUMN', N'WWI Supplier ID'
GO
EXEC sp_addextendedproperty N'Description', N'Transaction Type ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Movement_Staging', 'COLUMN', N'WWI Transaction Type ID'
GO
EXEC sp_addextendedproperty N'Description', N'Movement staging table', 'SCHEMA', N'Integration', 'TABLE', N'Movement_Staging', NULL, NULL
GO
ALTER TABLE [Integration].[Movement_Staging] SET (LOCK_ESCALATION = TABLE)
GO
