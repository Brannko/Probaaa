SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Integration].[StockHolding_Staging] (
		[Stock Holding Staging Key]     [bigint] IDENTITY(1, 1) NOT NULL,
		[Stock Item Key]                [int] NULL,
		[Quantity On Hand]              [int] NULL,
		[Bin Location]                  [nvarchar](20) COLLATE Latin1_General_100_CI_AS NULL,
		[Last Stocktake Quantity]       [int] NULL,
		[Last Cost Price]               [decimal](18, 2) NULL,
		[Reorder Level]                 [int] NULL,
		[Target Stock Level]            [int] NULL,
		[WWI Stock Item ID]             [int] NULL,
		CONSTRAINT [PK_Integration_StockHolding_Staging]
		PRIMARY KEY
		CLUSTERED
		([Stock Holding Staging Key])
	ON [USERDATA]
) ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Bin location (where is this stock in the warehouse)', 'SCHEMA', N'Integration', 'TABLE', N'StockHolding_Staging', 'COLUMN', N'Bin Location'
GO
EXEC sp_addextendedproperty N'Description', N'Unit cost when the stock item was last purchased', 'SCHEMA', N'Integration', 'TABLE', N'StockHolding_Staging', 'COLUMN', N'Last Cost Price'
GO
EXEC sp_addextendedproperty N'Description', N'Quantity present at last stocktake', 'SCHEMA', N'Integration', 'TABLE', N'StockHolding_Staging', 'COLUMN', N'Last Stocktake Quantity'
GO
EXEC sp_addextendedproperty N'Description', N'Quantity on hand', 'SCHEMA', N'Integration', 'TABLE', N'StockHolding_Staging', 'COLUMN', N'Quantity On Hand'
GO
EXEC sp_addextendedproperty N'Description', N'Quantity below which reordering should take place', 'SCHEMA', N'Integration', 'TABLE', N'StockHolding_Staging', 'COLUMN', N'Reorder Level'
GO
EXEC sp_addextendedproperty N'Description', N'DW key for a row in the Stock Holding fact', 'SCHEMA', N'Integration', 'TABLE', N'StockHolding_Staging', 'COLUMN', N'Stock Holding Staging Key'
GO
EXEC sp_addextendedproperty N'Description', N'Stock item being held', 'SCHEMA', N'Integration', 'TABLE', N'StockHolding_Staging', 'COLUMN', N'Stock Item Key'
GO
EXEC sp_addextendedproperty N'Description', N'Typical stock level held', 'SCHEMA', N'Integration', 'TABLE', N'StockHolding_Staging', 'COLUMN', N'Target Stock Level'
GO
EXEC sp_addextendedproperty N'Description', N'Stock Item ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'StockHolding_Staging', 'COLUMN', N'WWI Stock Item ID'
GO
EXEC sp_addextendedproperty N'Description', N'Stock holding staging table', 'SCHEMA', N'Integration', 'TABLE', N'StockHolding_Staging', NULL, NULL
GO
ALTER TABLE [Integration].[StockHolding_Staging] SET (LOCK_ESCALATION = TABLE)
GO
