SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Integration].[Order_Staging] (
		[Order Staging Key]       [bigint] IDENTITY(1, 1) NOT NULL,
		[City Key]                [int] NULL,
		[Customer Key]            [int] NULL,
		[Stock Item Key]          [int] NULL,
		[Order Date Key]          [date] NULL,
		[Picked Date Key]         [date] NULL,
		[Salesperson Key]         [int] NULL,
		[Picker Key]              [int] NULL,
		[WWI Order ID]            [int] NULL,
		[WWI Backorder ID]        [int] NULL,
		[Description]             [nvarchar](100) COLLATE Latin1_General_100_CI_AS NULL,
		[Package]                 [nvarchar](50) COLLATE Latin1_General_100_CI_AS NULL,
		[Quantity]                [int] NULL,
		[Unit Price]              [decimal](18, 2) NULL,
		[Tax Rate]                [decimal](18, 3) NULL,
		[Total Excluding Tax]     [decimal](18, 2) NULL,
		[Tax Amount]              [decimal](18, 2) NULL,
		[Total Including Tax]     [decimal](18, 2) NULL,
		[Lineage Key]             [int] NULL,
		[WWI City ID]             [int] NULL,
		[WWI Customer ID]         [int] NULL,
		[WWI Stock Item ID]       [int] NULL,
		[WWI Salesperson ID]      [int] NULL,
		[WWI Picker ID]           [int] NULL,
		[Last Modified When]      [datetime2](7) NULL,
		CONSTRAINT [PK_Integration_Order_Staging]
		PRIMARY KEY
		CLUSTERED
		([Order Staging Key])
	ON [USERDATA]
) ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'City for this order', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'City Key'
GO
EXEC sp_addextendedproperty N'Description', N'Customer for this order', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'Customer Key'
GO
EXEC sp_addextendedproperty N'Description', N'Description of the item supplied (Usually the stock item name but can be overridden)', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'Description', N'When this row was last modified', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'Last Modified When'
GO
EXEC sp_addextendedproperty N'Description', N'Lineage Key for the data load for this row', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'Lineage Key'
GO
EXEC sp_addextendedproperty N'Description', N'Order date for this order', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'Order Date Key'
GO
EXEC sp_addextendedproperty N'Description', N'DW key for a row in the Order fact', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'Order Staging Key'
GO
EXEC sp_addextendedproperty N'Description', N'Type of package to be supplied', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'Package'
GO
EXEC sp_addextendedproperty N'Description', N'Picked date for this order', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'Picked Date Key'
GO
EXEC sp_addextendedproperty N'Description', N'Picker for this order', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'Picker Key'
GO
EXEC sp_addextendedproperty N'Description', N'Quantity to be supplied', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'Quantity'
GO
EXEC sp_addextendedproperty N'Description', N'Salesperson for this order', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'Salesperson Key'
GO
EXEC sp_addextendedproperty N'Description', N'Stock item for this order', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'Stock Item Key'
GO
EXEC sp_addextendedproperty N'Description', N'Total amount of tax', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'Tax Amount'
GO
EXEC sp_addextendedproperty N'Description', N'Tax rate to be applied', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'Tax Rate'
GO
EXEC sp_addextendedproperty N'Description', N'Total amount excluding tax', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'Total Excluding Tax'
GO
EXEC sp_addextendedproperty N'Description', N'Total amount including tax', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'Total Including Tax'
GO
EXEC sp_addextendedproperty N'Description', N'Unit price to be charged', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'Unit Price'
GO
EXEC sp_addextendedproperty N'Description', N'BackorderID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'WWI Backorder ID'
GO
EXEC sp_addextendedproperty N'Description', N'City ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'WWI City ID'
GO
EXEC sp_addextendedproperty N'Description', N'Customer ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'WWI Customer ID'
GO
EXEC sp_addextendedproperty N'Description', N'OrderID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'WWI Order ID'
GO
EXEC sp_addextendedproperty N'Description', N'Picker person ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'WWI Picker ID'
GO
EXEC sp_addextendedproperty N'Description', N'Salesperson person ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'WWI Salesperson ID'
GO
EXEC sp_addextendedproperty N'Description', N'Stock Item ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', 'COLUMN', N'WWI Stock Item ID'
GO
EXEC sp_addextendedproperty N'Description', N'Order staging table', 'SCHEMA', N'Integration', 'TABLE', N'Order_Staging', NULL, NULL
GO
ALTER TABLE [Integration].[Order_Staging] SET (LOCK_ESCALATION = TABLE)
GO
