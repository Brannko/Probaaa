SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Integration].[Sale_Staging] (
		[Sale Staging Key]            [bigint] IDENTITY(1, 1) NOT NULL,
		[City Key]                    [int] NULL,
		[Customer Key]                [int] NULL,
		[Bill To Customer Key]        [int] NULL,
		[Stock Item Key]              [int] NULL,
		[Invoice Date Key]            [date] NULL,
		[Delivery Date Key]           [date] NULL,
		[Salesperson Key]             [int] NULL,
		[WWI Invoice ID]              [int] NULL,
		[Description]                 [nvarchar](100) COLLATE Latin1_General_100_CI_AS NULL,
		[Package]                     [nvarchar](50) COLLATE Latin1_General_100_CI_AS NULL,
		[Quantity]                    [int] NULL,
		[Unit Price]                  [decimal](18, 2) NULL,
		[Tax Rate]                    [decimal](18, 3) NULL,
		[Total Excluding Tax]         [decimal](18, 2) NULL,
		[Tax Amount]                  [decimal](18, 2) NULL,
		[Profit]                      [decimal](18, 2) NULL,
		[Total Including Tax]         [decimal](18, 2) NULL,
		[Total Dry Items]             [int] NULL,
		[Total Chiller Items]         [int] NULL,
		[WWI City ID]                 [int] NULL,
		[WWI Customer ID]             [int] NULL,
		[WWI Bill To Customer ID]     [int] NULL,
		[WWI Stock Item ID]           [int] NULL,
		[WWI Salesperson ID]          [int] NULL,
		[Last Modified When]          [datetime2](7) NULL,
		CONSTRAINT [PK_Integration_Sale_Staging]
		PRIMARY KEY
		CLUSTERED
		([Sale Staging Key])
	ON [USERDATA]
) ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Bill To Customer for this invoice', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'Bill To Customer Key'
GO
EXEC sp_addextendedproperty N'Description', N'City for this invoice', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'City Key'
GO
EXEC sp_addextendedproperty N'Description', N'Customer for this invoice', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'Customer Key'
GO
EXEC sp_addextendedproperty N'Description', N'Date that these items were delivered', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'Delivery Date Key'
GO
EXEC sp_addextendedproperty N'Description', N'Description of the item supplied (Usually the stock item name but can be overridden)', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'Description', N'Invoice date for this invoice', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'Invoice Date Key'
GO
EXEC sp_addextendedproperty N'Description', N'When this row was last modified', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'Last Modified When'
GO
EXEC sp_addextendedproperty N'Description', N'Type of package supplied', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'Package'
GO
EXEC sp_addextendedproperty N'Description', N'Total amount of profit', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'Profit'
GO
EXEC sp_addextendedproperty N'Description', N'Quantity supplied', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'Quantity'
GO
EXEC sp_addextendedproperty N'Description', N'DW key for a row in the Sale fact', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'Sale Staging Key'
GO
EXEC sp_addextendedproperty N'Description', N'Salesperson for this invoice', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'Salesperson Key'
GO
EXEC sp_addextendedproperty N'Description', N'Stock item for this invoice', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'Stock Item Key'
GO
EXEC sp_addextendedproperty N'Description', N'Total amount of tax', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'Tax Amount'
GO
EXEC sp_addextendedproperty N'Description', N'Tax rate applied', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'Tax Rate'
GO
EXEC sp_addextendedproperty N'Description', N'Total number of chiller items', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'Total Chiller Items'
GO
EXEC sp_addextendedproperty N'Description', N'Total number of dry items', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'Total Dry Items'
GO
EXEC sp_addextendedproperty N'Description', N'Total amount excluding tax', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'Total Excluding Tax'
GO
EXEC sp_addextendedproperty N'Description', N'Total amount including tax', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'Total Including Tax'
GO
EXEC sp_addextendedproperty N'Description', N'Unit price charged', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'Unit Price'
GO
EXEC sp_addextendedproperty N'Description', N'Bill to Customer ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'WWI Bill To Customer ID'
GO
EXEC sp_addextendedproperty N'Description', N'City ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'WWI City ID'
GO
EXEC sp_addextendedproperty N'Description', N'Customer ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'WWI Customer ID'
GO
EXEC sp_addextendedproperty N'Description', N'InvoiceID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'WWI Invoice ID'
GO
EXEC sp_addextendedproperty N'Description', N'Salesperson person ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'WWI Salesperson ID'
GO
EXEC sp_addextendedproperty N'Description', N'Stock Item ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', 'COLUMN', N'WWI Stock Item ID'
GO
EXEC sp_addextendedproperty N'Description', N'Sale staging table', 'SCHEMA', N'Integration', 'TABLE', N'Sale_Staging', NULL, NULL
GO
ALTER TABLE [Integration].[Sale_Staging] SET (LOCK_ESCALATION = TABLE)
GO
