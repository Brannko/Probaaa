SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Integration].[Transaction_Staging] (
		[Transaction Staging Key]         [bigint] IDENTITY(1, 1) NOT NULL,
		[Date Key]                        [date] NULL,
		[Customer Key]                    [int] NULL,
		[Bill To Customer Key]            [int] NULL,
		[Supplier Key]                    [int] NULL,
		[Transaction Type Key]            [int] NULL,
		[Payment Method Key]              [int] NULL,
		[WWI Customer Transaction ID]     [int] NULL,
		[WWI Supplier Transaction ID]     [int] NULL,
		[WWI Invoice ID]                  [int] NULL,
		[WWI Purchase Order ID]           [int] NULL,
		[Supplier Invoice Number]         [nvarchar](20) COLLATE Latin1_General_100_CI_AS NULL,
		[Total Excluding Tax]             [decimal](18, 2) NULL,
		[Tax Amount]                      [decimal](18, 2) NULL,
		[Total Including Tax]             [decimal](18, 2) NULL,
		[Outstanding Balance]             [decimal](18, 2) NULL,
		[Is Finalized]                    [bit] NULL,
		[WWI Customer ID]                 [int] NULL,
		[WWI Bill To Customer ID]         [int] NULL,
		[WWI Supplier ID]                 [int] NULL,
		[WWI Transaction Type ID]         [int] NULL,
		[WWI Payment Method ID]           [int] NULL,
		[Last Modified When]              [datetime2](7) NULL,
		CONSTRAINT [PK_Integration_Transaction_Staging]
		PRIMARY KEY
		CLUSTERED
		([Transaction Staging Key])
	ON [USERDATA]
) ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Bill to customer (if applicable)', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', 'COLUMN', N'Bill To Customer Key'
GO
EXEC sp_addextendedproperty N'Description', N'Customer (if applicable)', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', 'COLUMN', N'Customer Key'
GO
EXEC sp_addextendedproperty N'Description', N'Transaction date', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', 'COLUMN', N'Date Key'
GO
EXEC sp_addextendedproperty N'Description', N'Has this transaction been finalized?', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', 'COLUMN', N'Is Finalized'
GO
EXEC sp_addextendedproperty N'Description', N'When this row was last modified', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', 'COLUMN', N'Last Modified When'
GO
EXEC sp_addextendedproperty N'Description', N'Amount still outstanding for this transaction', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', 'COLUMN', N'Outstanding Balance'
GO
EXEC sp_addextendedproperty N'Description', N'Payment method (if applicable)', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', 'COLUMN', N'Payment Method Key'
GO
EXEC sp_addextendedproperty N'Description', N'Supplier invoice number (if applicable)', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', 'COLUMN', N'Supplier Invoice Number'
GO
EXEC sp_addextendedproperty N'Description', N'Supplier (if applicable)', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', 'COLUMN', N'Supplier Key'
GO
EXEC sp_addextendedproperty N'Description', N'Total amount of tax', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', 'COLUMN', N'Tax Amount'
GO
EXEC sp_addextendedproperty N'Description', N'Total amount excluding tax', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', 'COLUMN', N'Total Excluding Tax'
GO
EXEC sp_addextendedproperty N'Description', N'Total amount including tax', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', 'COLUMN', N'Total Including Tax'
GO
EXEC sp_addextendedproperty N'Description', N'DW key for a row in the Transaction fact', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', 'COLUMN', N'Transaction Staging Key'
GO
EXEC sp_addextendedproperty N'Description', N'Type of transaction', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', 'COLUMN', N'Transaction Type Key'
GO
EXEC sp_addextendedproperty N'Description', N'Bill to Customer ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', 'COLUMN', N'WWI Bill To Customer ID'
GO
EXEC sp_addextendedproperty N'Description', N'Customer ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', 'COLUMN', N'WWI Customer ID'
GO
EXEC sp_addextendedproperty N'Description', N'Customer transaction ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', 'COLUMN', N'WWI Customer Transaction ID'
GO
EXEC sp_addextendedproperty N'Description', N'Invoice ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', 'COLUMN', N'WWI Invoice ID'
GO
EXEC sp_addextendedproperty N'Description', N'Payment method ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', 'COLUMN', N'WWI Payment Method ID'
GO
EXEC sp_addextendedproperty N'Description', N'Purchase order ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', 'COLUMN', N'WWI Purchase Order ID'
GO
EXEC sp_addextendedproperty N'Description', N'Supplier ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', 'COLUMN', N'WWI Supplier ID'
GO
EXEC sp_addextendedproperty N'Description', N'Supplier transaction ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', 'COLUMN', N'WWI Supplier Transaction ID'
GO
EXEC sp_addextendedproperty N'Description', N'Transaction Type ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', 'COLUMN', N'WWI Transaction Type ID'
GO
EXEC sp_addextendedproperty N'Description', N'Transaction staging table', 'SCHEMA', N'Integration', 'TABLE', N'Transaction_Staging', NULL, NULL
GO
ALTER TABLE [Integration].[Transaction_Staging] SET (LOCK_ESCALATION = TABLE)
GO
