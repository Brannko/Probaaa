SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Integration].[Customer_Staging] (
		[Customer Staging Key]     [int] IDENTITY(1, 1) NOT NULL,
		[WWI Customer ID]          [int] NOT NULL,
		[Customer]                 [nvarchar](100) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[Bill To Customer]         [nvarchar](100) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[Category]                 [nvarchar](50) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[Buying Group]             [nvarchar](50) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[Primary Contact]          [nvarchar](50) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[Postal Code]              [nvarchar](10) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[Valid From]               [datetime2](7) NOT NULL,
		[Valid To]                 [datetime2](7) NOT NULL,
		CONSTRAINT [PK_Integration_Customer_Staging]
		PRIMARY KEY
		CLUSTERED
		([Customer Staging Key])
	ON [USERDATA]
) ON [USERDATA]
GO
CREATE NONCLUSTERED INDEX [IX_Integration_Customer_Staging_WWI_Customer_ID]
	ON [Integration].[Customer_Staging] ([WWI Customer ID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Allows quickly locating by WWI Customer ID', 'SCHEMA', N'Integration', 'TABLE', N'Customer_Staging', 'INDEX', N'IX_Integration_Customer_Staging_WWI_Customer_ID'
GO
EXEC sp_addextendedproperty N'Description', N'Bill to customer''s full name', 'SCHEMA', N'Integration', 'TABLE', N'Customer_Staging', 'COLUMN', N'Bill To Customer'
GO
EXEC sp_addextendedproperty N'Description', N'Customer''s buying group', 'SCHEMA', N'Integration', 'TABLE', N'Customer_Staging', 'COLUMN', N'Buying Group'
GO
EXEC sp_addextendedproperty N'Description', N'Customer''s category', 'SCHEMA', N'Integration', 'TABLE', N'Customer_Staging', 'COLUMN', N'Category'
GO
EXEC sp_addextendedproperty N'Description', N'Row ID within the staging table', 'SCHEMA', N'Integration', 'TABLE', N'Customer_Staging', 'COLUMN', N'Customer Staging Key'
GO
EXEC sp_addextendedproperty N'Description', N'Customer''s full name (usually a trading name)', 'SCHEMA', N'Integration', 'TABLE', N'Customer_Staging', 'COLUMN', N'Customer'
GO
EXEC sp_addextendedproperty N'Description', N'Delivery postal code for the customer', 'SCHEMA', N'Integration', 'TABLE', N'Customer_Staging', 'COLUMN', N'Postal Code'
GO
EXEC sp_addextendedproperty N'Description', N'Primary contact', 'SCHEMA', N'Integration', 'TABLE', N'Customer_Staging', 'COLUMN', N'Primary Contact'
GO
EXEC sp_addextendedproperty N'Description', N'Valid from this date and time', 'SCHEMA', N'Integration', 'TABLE', N'Customer_Staging', 'COLUMN', N'Valid From'
GO
EXEC sp_addextendedproperty N'Description', N'Valid until this date and time', 'SCHEMA', N'Integration', 'TABLE', N'Customer_Staging', 'COLUMN', N'Valid To'
GO
EXEC sp_addextendedproperty N'Description', N'Numeric ID used for reference to a customer within the WWI database', 'SCHEMA', N'Integration', 'TABLE', N'Customer_Staging', 'COLUMN', N'WWI Customer ID'
GO
EXEC sp_addextendedproperty N'Description', N'Customer staging table', 'SCHEMA', N'Integration', 'TABLE', N'Customer_Staging', NULL, NULL
GO
ALTER TABLE [Integration].[Customer_Staging] SET (LOCK_ESCALATION = TABLE)
GO
