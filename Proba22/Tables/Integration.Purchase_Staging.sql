SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Integration].[Purchase_Staging] (
		[Purchase Staging Key]      [bigint] IDENTITY(1, 1) NOT NULL,
		[Date Key]                  [date] NULL,
		[Supplier Key]              [int] NULL,
		[Stock Item Key]            [int] NULL,
		[WWI Purchase Order ID]     [int] NULL,
		[Ordered Outers]            [int] NULL,
		[Ordered Quantity]          [int] NULL,
		[Received Outers]           [int] NULL,
		[Package]                   [nvarchar](50) COLLATE Latin1_General_100_CI_AS NULL,
		[Is Order Finalized]        [bit] NULL,
		[WWI Supplier ID]           [int] NULL,
		[WWI Stock Item ID]         [int] NULL,
		[Last Modified When]        [datetime2](7) NULL,
		CONSTRAINT [PK_Integration_Purchase_Staging]
		PRIMARY KEY
		CLUSTERED
		([Purchase Staging Key])
	ON [USERDATA]
) ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Purchase order date', 'SCHEMA', N'Integration', 'TABLE', N'Purchase_Staging', 'COLUMN', N'Date Key'
GO
EXEC sp_addextendedproperty N'Description', N'Is this purchase order now finalized?', 'SCHEMA', N'Integration', 'TABLE', N'Purchase_Staging', 'COLUMN', N'Is Order Finalized'
GO
EXEC sp_addextendedproperty N'Description', N'When this row was last modified', 'SCHEMA', N'Integration', 'TABLE', N'Purchase_Staging', 'COLUMN', N'Last Modified When'
GO
EXEC sp_addextendedproperty N'Description', N'Quantity of outers (ordering packages)', 'SCHEMA', N'Integration', 'TABLE', N'Purchase_Staging', 'COLUMN', N'Ordered Outers'
GO
EXEC sp_addextendedproperty N'Description', N'Quantity of inners (selling packages)', 'SCHEMA', N'Integration', 'TABLE', N'Purchase_Staging', 'COLUMN', N'Ordered Quantity'
GO
EXEC sp_addextendedproperty N'Description', N'Package ordered', 'SCHEMA', N'Integration', 'TABLE', N'Purchase_Staging', 'COLUMN', N'Package'
GO
EXEC sp_addextendedproperty N'Description', N'DW key for a row in the Purchase fact', 'SCHEMA', N'Integration', 'TABLE', N'Purchase_Staging', 'COLUMN', N'Purchase Staging Key'
GO
EXEC sp_addextendedproperty N'Description', N'Received outers (so far)', 'SCHEMA', N'Integration', 'TABLE', N'Purchase_Staging', 'COLUMN', N'Received Outers'
GO
EXEC sp_addextendedproperty N'Description', N'Stock item for this purchase order', 'SCHEMA', N'Integration', 'TABLE', N'Purchase_Staging', 'COLUMN', N'Stock Item Key'
GO
EXEC sp_addextendedproperty N'Description', N'Supplier for this purchase order', 'SCHEMA', N'Integration', 'TABLE', N'Purchase_Staging', 'COLUMN', N'Supplier Key'
GO
EXEC sp_addextendedproperty N'Description', N'Purchase order ID in source system ', 'SCHEMA', N'Integration', 'TABLE', N'Purchase_Staging', 'COLUMN', N'WWI Purchase Order ID'
GO
EXEC sp_addextendedproperty N'Description', N'Stock Item ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Purchase_Staging', 'COLUMN', N'WWI Stock Item ID'
GO
EXEC sp_addextendedproperty N'Description', N'Supplier ID in source system', 'SCHEMA', N'Integration', 'TABLE', N'Purchase_Staging', 'COLUMN', N'WWI Supplier ID'
GO
EXEC sp_addextendedproperty N'Description', N'Purchase staging table', 'SCHEMA', N'Integration', 'TABLE', N'Purchase_Staging', NULL, NULL
GO
ALTER TABLE [Integration].[Purchase_Staging] SET (LOCK_ESCALATION = TABLE)
GO
