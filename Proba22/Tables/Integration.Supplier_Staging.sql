SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Integration].[Supplier_Staging] (
		[Supplier Staging Key]     [int] IDENTITY(1, 1) NOT NULL,
		[WWI Supplier ID]          [int] NOT NULL,
		[Supplier]                 [nvarchar](100) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[Category]                 [nvarchar](50) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[Primary Contact]          [nvarchar](50) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[Supplier Reference]       [nvarchar](20) COLLATE Latin1_General_100_CI_AS NULL,
		[Payment Days]             [int] NOT NULL,
		[Postal Code]              [nvarchar](10) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[Valid From]               [datetime2](7) NOT NULL,
		[Valid To]                 [datetime2](7) NOT NULL,
		CONSTRAINT [PK_Integration_Supplier_Staging]
		PRIMARY KEY
		CLUSTERED
		([Supplier Staging Key])
	ON [USERDATA]
) ON [USERDATA]
GO
CREATE NONCLUSTERED INDEX [IX_Integration_Supplier_Staging_WWI_Supplier_ID]
	ON [Integration].[Supplier_Staging] ([WWI Supplier ID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Allows quickly locating by WWI Supplier ID', 'SCHEMA', N'Integration', 'TABLE', N'Supplier_Staging', 'INDEX', N'IX_Integration_Supplier_Staging_WWI_Supplier_ID'
GO
EXEC sp_addextendedproperty N'Description', N'Supplier''s category', 'SCHEMA', N'Integration', 'TABLE', N'Supplier_Staging', 'COLUMN', N'Category'
GO
EXEC sp_addextendedproperty N'Description', N'Number of days for payment of an invoice (ie payment terms)', 'SCHEMA', N'Integration', 'TABLE', N'Supplier_Staging', 'COLUMN', N'Payment Days'
GO
EXEC sp_addextendedproperty N'Description', N'Delivery postal code for the supplier', 'SCHEMA', N'Integration', 'TABLE', N'Supplier_Staging', 'COLUMN', N'Postal Code'
GO
EXEC sp_addextendedproperty N'Description', N'Primary contact', 'SCHEMA', N'Integration', 'TABLE', N'Supplier_Staging', 'COLUMN', N'Primary Contact'
GO
EXEC sp_addextendedproperty N'Description', N'Supplier reference for our organization (might be our account number at the supplier)', 'SCHEMA', N'Integration', 'TABLE', N'Supplier_Staging', 'COLUMN', N'Supplier Reference'
GO
EXEC sp_addextendedproperty N'Description', N'Row ID within the staging table', 'SCHEMA', N'Integration', 'TABLE', N'Supplier_Staging', 'COLUMN', N'Supplier Staging Key'
GO
EXEC sp_addextendedproperty N'Description', N'Supplier''s full name (usually a trading name)', 'SCHEMA', N'Integration', 'TABLE', N'Supplier_Staging', 'COLUMN', N'Supplier'
GO
EXEC sp_addextendedproperty N'Description', N'Valid from this date and time', 'SCHEMA', N'Integration', 'TABLE', N'Supplier_Staging', 'COLUMN', N'Valid From'
GO
EXEC sp_addextendedproperty N'Description', N'Valid until this date and time', 'SCHEMA', N'Integration', 'TABLE', N'Supplier_Staging', 'COLUMN', N'Valid To'
GO
EXEC sp_addextendedproperty N'Description', N'Numeric ID used for reference to a supplier within the WWI database', 'SCHEMA', N'Integration', 'TABLE', N'Supplier_Staging', 'COLUMN', N'WWI Supplier ID'
GO
EXEC sp_addextendedproperty N'Description', N'Supplier staging table', 'SCHEMA', N'Integration', 'TABLE', N'Supplier_Staging', NULL, NULL
GO
ALTER TABLE [Integration].[Supplier_Staging] SET (LOCK_ESCALATION = TABLE)
GO
