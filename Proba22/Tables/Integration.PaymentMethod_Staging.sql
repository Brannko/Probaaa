SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Integration].[PaymentMethod_Staging] (
		[Payment Method Staging Key]     [int] IDENTITY(1, 1) NOT NULL,
		[WWI Payment Method ID]          [int] NOT NULL,
		[Payment Method]                 [nvarchar](50) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[Valid From]                     [datetime2](7) NOT NULL,
		[Valid To]                       [datetime2](7) NOT NULL,
		CONSTRAINT [PK_Integration_PaymentMethod_Staging]
		PRIMARY KEY
		CLUSTERED
		([Payment Method Staging Key])
	ON [USERDATA]
) ON [USERDATA]
GO
CREATE NONCLUSTERED INDEX [IX_Integration_PaymentMethod_Staging_WWI_Payment_Method_ID]
	ON [Integration].[PaymentMethod_Staging] ([WWI Payment Method ID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Allows quickly locating by WWI Payment Method ID', 'SCHEMA', N'Integration', 'TABLE', N'PaymentMethod_Staging', 'INDEX', N'IX_Integration_PaymentMethod_Staging_WWI_Payment_Method_ID'
GO
EXEC sp_addextendedproperty N'Description', N'Row ID within the staging table', 'SCHEMA', N'Integration', 'TABLE', N'PaymentMethod_Staging', 'COLUMN', N'Payment Method Staging Key'
GO
EXEC sp_addextendedproperty N'Description', N'Payment method name', 'SCHEMA', N'Integration', 'TABLE', N'PaymentMethod_Staging', 'COLUMN', N'Payment Method'
GO
EXEC sp_addextendedproperty N'Description', N'Valid from this date and time', 'SCHEMA', N'Integration', 'TABLE', N'PaymentMethod_Staging', 'COLUMN', N'Valid From'
GO
EXEC sp_addextendedproperty N'Description', N'Valid until this date and time', 'SCHEMA', N'Integration', 'TABLE', N'PaymentMethod_Staging', 'COLUMN', N'Valid To'
GO
EXEC sp_addextendedproperty N'Description', N'Numeric ID for the payment method in the WWI database', 'SCHEMA', N'Integration', 'TABLE', N'PaymentMethod_Staging', 'COLUMN', N'WWI Payment Method ID'
GO
EXEC sp_addextendedproperty N'Description', N'Payment method staging table', 'SCHEMA', N'Integration', 'TABLE', N'PaymentMethod_Staging', NULL, NULL
GO
ALTER TABLE [Integration].[PaymentMethod_Staging] SET (LOCK_ESCALATION = TABLE)
GO
