SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Integration].[TransactionType_Staging] (
		[Transaction Type Staging Key]     [int] IDENTITY(1, 1) NOT NULL,
		[WWI Transaction Type ID]          [int] NOT NULL,
		[Transaction Type]                 [nvarchar](50) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[Valid From]                       [datetime2](7) NOT NULL,
		[Valid To]                         [datetime2](7) NOT NULL,
		CONSTRAINT [PK_Integration_TransactionType_Staging]
		PRIMARY KEY
		CLUSTERED
		([Transaction Type Staging Key])
	ON [USERDATA]
) ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Row ID within the staging table', 'SCHEMA', N'Integration', 'TABLE', N'TransactionType_Staging', 'COLUMN', N'Transaction Type Staging Key'
GO
EXEC sp_addextendedproperty N'Description', N'Full name of the transaction type', 'SCHEMA', N'Integration', 'TABLE', N'TransactionType_Staging', 'COLUMN', N'Transaction Type'
GO
EXEC sp_addextendedproperty N'Description', N'Valid from this date and time', 'SCHEMA', N'Integration', 'TABLE', N'TransactionType_Staging', 'COLUMN', N'Valid From'
GO
EXEC sp_addextendedproperty N'Description', N'Valid until this date and time', 'SCHEMA', N'Integration', 'TABLE', N'TransactionType_Staging', 'COLUMN', N'Valid To'
GO
EXEC sp_addextendedproperty N'Description', N'Numeric ID used for reference to a transaction type within the WWI database', 'SCHEMA', N'Integration', 'TABLE', N'TransactionType_Staging', 'COLUMN', N'WWI Transaction Type ID'
GO
EXEC sp_addextendedproperty N'Description', N'Transaction type staging table', 'SCHEMA', N'Integration', 'TABLE', N'TransactionType_Staging', NULL, NULL
GO
ALTER TABLE [Integration].[TransactionType_Staging] SET (LOCK_ESCALATION = TABLE)
GO
