SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Integration].[Employee_Staging] (
		[Employee Staging Key]     [int] IDENTITY(1, 1) NOT NULL,
		[WWI Employee ID]          [int] NOT NULL,
		[Employee]                 [nvarchar](50) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[Preferred Name]           [nvarchar](50) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[Is Salesperson]           [bit] NOT NULL,
		[Photo]                    [varbinary](max) NULL,
		[Valid From]               [datetime2](7) NOT NULL,
		[Valid To]                 [datetime2](7) NOT NULL,
		CONSTRAINT [PK_Integration_Employee_Staging]
		PRIMARY KEY
		CLUSTERED
		([Employee Staging Key])
	ON [USERDATA]
) ON [USERDATA] TEXTIMAGE_ON [USERDATA]
GO
CREATE NONCLUSTERED INDEX [IX_Integration_Employee_Staging_WWI_Employee_ID]
	ON [Integration].[Employee_Staging] ([WWI Employee ID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Allows quickly locating by WWI Employee ID', 'SCHEMA', N'Integration', 'TABLE', N'Employee_Staging', 'INDEX', N'IX_Integration_Employee_Staging_WWI_Employee_ID'
GO
EXEC sp_addextendedproperty N'Description', N'Row ID within the staging table', 'SCHEMA', N'Integration', 'TABLE', N'Employee_Staging', 'COLUMN', N'Employee Staging Key'
GO
EXEC sp_addextendedproperty N'Description', N'Full name for this person', 'SCHEMA', N'Integration', 'TABLE', N'Employee_Staging', 'COLUMN', N'Employee'
GO
EXEC sp_addextendedproperty N'Description', N'Is this person a staff salesperson?', 'SCHEMA', N'Integration', 'TABLE', N'Employee_Staging', 'COLUMN', N'Is Salesperson'
GO
EXEC sp_addextendedproperty N'Description', N'Photo of this person', 'SCHEMA', N'Integration', 'TABLE', N'Employee_Staging', 'COLUMN', N'Photo'
GO
EXEC sp_addextendedproperty N'Description', N'Name that this person prefers to be called', 'SCHEMA', N'Integration', 'TABLE', N'Employee_Staging', 'COLUMN', N'Preferred Name'
GO
EXEC sp_addextendedproperty N'Description', N'Valid from this date and time', 'SCHEMA', N'Integration', 'TABLE', N'Employee_Staging', 'COLUMN', N'Valid From'
GO
EXEC sp_addextendedproperty N'Description', N'Valid until this date and time', 'SCHEMA', N'Integration', 'TABLE', N'Employee_Staging', 'COLUMN', N'Valid To'
GO
EXEC sp_addextendedproperty N'Description', N'Numeric ID (PersonID) in the WWI database', 'SCHEMA', N'Integration', 'TABLE', N'Employee_Staging', 'COLUMN', N'WWI Employee ID'
GO
EXEC sp_addextendedproperty N'Description', N'Employee staging table', 'SCHEMA', N'Integration', 'TABLE', N'Employee_Staging', NULL, NULL
GO
ALTER TABLE [Integration].[Employee_Staging] SET (LOCK_ESCALATION = TABLE)
GO
